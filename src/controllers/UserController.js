const { User } = require('../models/User')

module.exports = {

    async index (req, resp) {
        const user = new User()  
        let aux = await user.get({where: {name: 'admin'}})

        return resp.json({
            statusCode: 200,
            body: aux
        });

        // return resp.json({
        //     statusCode: 500,
        //     body: "error"
        // });

    },

    async store (req, resp) {
        
    },

    async destroy (req, resp) {
        
    },

    async update(){
        
    },

};
