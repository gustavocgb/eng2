const PrismaClient = require('@prisma/client').PrismaClient

class Prisma {

    prisma = new PrismaClient()

    async GetFirst(model, params) {
        let query
        let app
        let objQuery

        app = getApp(model, this.prisma)
        objQuery = getParamsApp(params)        
        query = await app.prisma.findFirst(objQuery)
        
        return query
    }

//     async GetMany(application: string, params: getParams): Promise<any> {
//         let query: any
//         let app: any
//         let objQuery: any
//         let array: any = []

//         app = getApp(application, this.prisma)
//         objQuery = getParamsApp(params, app)
//         query = await app.prisma.findMany(objQuery)

//         for (let obj of query) {
//             array.push(app.mapper.modelToEntity(obj))
//         }
//         return array
//     }

//     async Update(application: string, params: updateParams): Promise<any> {
//         let query: any
//         let app: any
//         let objQuery: any

//         app = getApp(application, this.prisma)
//         objQuery = updateParamsApp(params, app)
//         query = await app.prisma.update(objQuery)

//         return app.mapper.modelToEntity(query)
//     }

//     async Count(application: string, params: getParams): Promise<any> {
//         let query: any
//         let app: any
//         let objQuery: any

//         app = getApp(application, this.prisma)
//         objQuery = getParamsApp(params, app)
//         query = await app.prisma.count(objQuery)

//         if (objQuery.select) return app.mapper.modelToEntity(query)
//         return query
//     }

//     async GetUnique(application: string, params: getParams): Promise<any> {
//         return Promise.resolve(undefined);
//     }

//     async Post(application: string, params: any): Promise<any> {
//         return Promise.resolve(undefined);
//     }

//     async Delete(application: string, params: any): Promise<any> {
//         return Promise.resolve(undefined);
//     }
}

const getApp = (model, prisma) => {
    let app
    const config = [
        {
            model: 'User',
            prisma: prisma.User
        },{
            model: 'HelthPost',
            prisma: prisma.HelthPost
        },{
            model: 'Nurse',
            prisma: prisma.Nurse
        },{
            model: 'Patient',
            prisma: prisma.Patient
        },{
            model: 'Secretary',
            prisma: prisma.Secretary
        },{
            model: 'Vaccine',
            prisma: prisma.Vaccine
        }
    ]
    config.forEach(obj => {
        if (obj.model === model) app = obj
    })
    return app
}

const getParamsApp = (params) => {
    let objQuery = {
        where: params.where,
        cursor: params.cursor,
        skip: params.skip,
        select: params.select,
        orderBy: params.orderBy,
        take: params.take,
        or: params.or,
    }
    return objQuery
}

// const updateParamsApp = (params: updateParams, app: any) => {
//     let objQuery: updateParams = {
//         where: params.where?app.mapper.entityToModel(params.where):undefined,
//         data: params.data?app.mapper.entityToModel(params.data):undefined,
//     }
//     return objQuery
// }

module.exports = { Prisma }