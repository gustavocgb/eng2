const { Prisma } = require('../service/db/tech/postgresql/prisma/Prisma')

class User {
    
    sqlDrive = new Prisma()

    async create(data) {
        let params
        params = {
            value: data,
        }
        return await this.sqlDrive.Post("User", params);
    }

    async remove(data) {
        let params
        params = {
            id: data?.id,
        }
        return await this.sqlDrive.Delete("User", params);
    }

    async get(data) {
        let params
        params = {
            select: data?.select?.where,
            where: data?.where,
            orderBy: data?.orderBy?.where,
            skip: data?.skip,
            take: data?.take,
            cursor: data?.reference?.id,
        }
        return await this.sqlDrive.GetFirst("User", params);
    }

    async getAll(data){
        let params
        params = {
            select: data?.select?.where,
            where: data?.where,
            orderBy: data?.orderBy?.where,
            skip: data?.skip,
            take: data?.take,
            cursor: data?.reference?.id,
        };
        return await this.sqlDrive.GetMany("User", params);
    }

    async getCount(data){
        let params
        params = {
            select: data?.select?.where,
            where: data?.where,
            orderBy: data?.orderBy?.where,
            skip: data?.skip,
            take: data?.take,
            cursor: data?.reference?.id,
        }
        const response = await this.sqlDrive.Count("User", params);
        if (params.select) {
            let count = null;
            Object.entries(response).forEach((obj) => {
                if (obj[1]) count = obj[1];
            });
            return count;
        }
        return response;
    }

    async update(data){
        let params
        params = {
            where: data?.id,
            data: data?.where,
        };
        return await this.sqlDrive.Update("User", params);
    }
}

module.exports = { User }