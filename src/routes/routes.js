const { Router } = require('express');
const routes = Router();

const User = require('./User');

// default
routes.get('/', (req, res) => {
    res.send({ message: 'Hello World' });
});

// user
User.user(routes);


module.exports = routes;