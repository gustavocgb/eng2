const UserController = require('../controllers/UserController');
module.exports = {
    user(routes){
        routes.get('/user', UserController.index);
        routes.post('/user', UserController.store);
        routes.put('/user', UserController.update);
        routes.delete('/user', UserController.destroy);
    }
}