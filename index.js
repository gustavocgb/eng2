const express = require('express');
const cors = require('cors');
let port = process.env.PORT || 3000;

const routes = require('./src/routes/routes');
const app = express();

app.use(cors()) // libera acesso a todo tipo de aplicacao
app.use(express.json());
app.use(routes);
app.listen(port, () => {
  console.log("Server is running in port 3000");
});

module.exports = {
  app
}



